# Single-Cycle-LC-3b-Processor
(Implementation of MP2 in ECE411)

Single-Cycle LC-3bα Processor using SystemVerilog with 128-bit unified 2-way cache and simulated using Altera-Modelsim with a testbench.

Instruction Set is the same as the the standard [LC-3 instruction set](http://users.ece.utexas.edu/~patt/07s.360N/handouts/360n.appC.pdf), excluding the RTI instruction. Must supply own LC3-b assembled memory.lst in mp2/simulation/modelsim in order to run a program and simulate using modelsim.

Cache is a simple Unified 2-Way Set-Associative Cache, using LRU / write-back with write allocate policy.
