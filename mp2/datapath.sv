import lc3b_types::*;

//external signals
module datapath
(
	/////CLOCK/////
	input clk,


	////DATAPATH - CPU/////
	/*pc control signals */
	input logic pcmux_sel1,
	input logic pcmux_sel0,
	input logic load_pc,
	 
	//adj
	input logic adjmux_sel, // normal branch adj9 to adj adder, or jsr offset to adder

	//mar
	input logic marmux_sel1,  //mem addr from offset or from pc?
	input logic marmux_sel0,  //mem addr from offset or from pc?
	input logic load_mar, //get mar (addr) or not

	//mdr
	input logic mdrmux_sel, //load alu result or data from M[MAR] into MDR?
	input logic load_mdr, //get mdr (data) or not

	//alu
	input lc3b_aluop aluop, //which operation to do
	input logic alumux_sel2, //sr2 or offset or imm (bit 1)
	input logic alumux_sel1, //sr2 or offset or imm (bit 1)
	input logic alumux_sel0, //sr2 or offset or imm (bit 0)

	//reg_file / muxes
	input logic load_regfile, //get regfile data or not
	input logic r7mux_sel, // norm dest or r7
	input logic storemux_sel, //tells if we should use sr1 or dest reg
	input logic regfilemux_sel2, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 1)
	input logic regfilemux_sel1, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 1)
	input logic regfilemux_sel0, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 0)

	//ir
	input logic load_ir, // cpu asks to get info in instruction reg or not?
	output lc3b_opcode opcode, //send opcode to cpu
	output logic imm_sel, //tells control whether to use imm val
	output logic jsr_sel, //tells control whether to use pc offset for jsr
	output logic a_sig, //arithmetic shift?
	output logic d_sig, //right or left shift?

	//cccomp
	output logic branch_enable, //tells cpu whether to branch or not

	//cc
	input logic load_cc, // cpu tells to change to new cc or not

	////DATAPATH - MEM/////
	output lc3b_word mem_wdata, //output from MDR to write to memory
	input lc3b_word mem_rdata, //retrieved data from M[MAR]

	////datapath - MEM - CTRL//
	output lc3b_word mem_address //sends memory to be accessed
);

/* declare internal signals */
lc3b_word pcmux_out; // pc mux out (+2, alu out (reg val), offset, trap)
lc3b_word pc_out; // pc reg out
lc3b_word pc_plus2_out; //next address
lc3b_word adj11_out; //offset for jsr jumps
lc3b_word adj9_out; //offset for branches
lc3b_word adj_adder_out; //pc + jump offset

lc3b_word adjmux_out; // normal branch adj9 to adj adder, or jsr offset to adder

lc3b_word marmux_out; //marmux addr to get (to MAR)
lc3b_word mdrmux_out; //mdrmux addr to get (MDR)

lc3b_reg storemux_out; //gives sr1 or dest reg no.
lc3b_reg r7mux_out; //given dest reg or r7

lc3b_word regfile_sr1_out; //sr1 outputted by regfile for alu
lc3b_word regfile_sr2_out; //sr2 outputted by regfile for alu
lc3b_word regfilemux_out; //mux between what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address)
lc3b_word alumux_out; //sr2 or offset outputted by regfile
lc3b_word adj6_out; //offset for ld/str commands
lc3b_word alu_out; //output of alu (to marmux, mdrmux, regfilemux)

lc3b_imm5 ir_imm5_out; // the five bits from IR used to generate imm value
lc3b_imm4 ir_imm4_out; // the four bits from IR used to generate imm value
lc3b_trap8 ir_trap8_out; // the 8 bits from IR used to generate trap addr
lc3b_word imm5_out; //SEXT(imm value) -> ALU
lc3b_word imm4_out; //SEXT(imm value) -> ALU
lc3b_word imm6_out; //SEXT(imm value) -> ALU

logic immmux_sel; //5th lower bit of IR used to check if imm value
lc3b_word immmux_out; //sr2 or immediate value

lc3b_reg ir_sr1_out; //sr1 register number from IR for reg -> alu (or if not used, dest)
lc3b_reg ir_sr2_out; //sr2 register number from IR for reg -> alu
lc3b_reg ir_dest_out; //dest reg number from IR for reg -> alu or for cc branching calc
lc3b_offset6 ir_off6_out; // 6bit offset from IR for alu
lc3b_offset9 ir_off9_out; // 9bit offset from IR for alu
lc3b_offset11 ir_off11_out; // 11bit offset from IR for alu

lc3b_nzp gencc_out; //generated cc code
lc3b_nzp cc_reg_out; //stores a generated cc code

lc3b_word trapzext_out; // trap code zexted and << 1

logic [7:0] bytemux_out; //lower / higher bits from MDR
lc3b_word bytezext_out; //zexts byte gotten from MDR, forwards to register mux

/*
 * PC mux init
 */
mux4 pcmux
(
	.sel1(pcmux_sel1), .sel0(pcmux_sel0),
	.a(pc_plus2_out), .b(alu_out), .c(adj_adder_out), .d(mem_wdata),
	.f(pcmux_out)
);

//pc reg init
register pc
(
	.clk,
	.load(load_pc),
	.in(pcmux_out),
	.out(pc_out)
);

//pc next address (+2 unit init)
plus2 pc_plus2
(
	.in(pc_out),
	.out(pc_plus2_out)
);

// mux to decide to jump or not init
mux2 adjmux
(
	.sel(adjmux_sel),
	.a(adj11_out), .b(adj9_out),
	.f(adjmux_out)
);

//adj9 unit for pc branches
adj #(.width(9)) adj9
(
	.in(ir_off9_out),
	.out(adj9_out)
);

//adj11 unit for pc jumps
adj #(.width(11)) adj11
(
	.in(ir_off11_out),
	.out(adj11_out)
);

adder adj_adder
(
	.x(adjmux_out),
	.y(pc_out),
	.sum(adj_adder_out)
);

/*mar control init*/
mux4 marmux
(
	.sel1(marmux_sel1),
	.sel0(marmux_sel0),
	.a(alu_out), .b(pc_out), .c(trapzext_out), .d(mem_rdata),
	.f(marmux_out)
);

//mar reg
register mar_reg
(
	.clk,
	.load(load_mar),
	.in(marmux_out),
	.out(mem_address)
);

/*mdr copntrol init*/
mux2 mdrmux
(
	.sel(mdrmux_sel),
	.a(alu_out), .b(mem_rdata),
	.f(mdrmux_out)
);

//mdr reg init
register mdr_reg
(
	.clk,
	.load(load_mdr),
	.in(mdrmux_out),
	.out(mem_wdata)
);

//the alu init
alu alu_unit
(
	.aluop(aluop),
	.a(regfile_sr1_out), .b(alumux_out),
	.f(alu_out)
);

/*alu sr2 control mux init*/
mux5 alumux
(
	.sel2(alumux_sel2),
	.sel1(alumux_sel1),
	.sel0(alumux_sel0),
	.a(regfile_sr2_out),
	.b(adj6_out),
	.c(imm5_out),
	.d(imm6_out),
	.e(imm4_out),
	.f(alumux_out)
);

//imm unit to get immediate value
imm #(.width(5)) imm5
(
	.in(ir_imm5_out),
	.out(imm5_out)
);

//imm unit to get immediate value
imm #(.width(6)) imm6
(
	.in(ir_off6_out),
	.out(imm6_out)
);

//imm unit to get immediate value
zext #(.in_width(4), .out_width(16)) imm4
(
	.in(ir_imm4_out),
	.out(imm4_out)
);

r7mux r7mux_unit
(
	.sel(r7mux_sel),
	.normal_dest(ir_dest_out),
	.f(r7mux_out)
);

/*alu sr2 control mux init*/
regfile regfile_unit
(
	.clk,
	.load(load_regfile),
	.in(regfilemux_out),
	.src_a(storemux_out), .src_b(ir_sr2_out),
	.dest(r7mux_out),
	.reg_a(regfile_sr1_out), .reg_b(regfile_sr2_out)
);

mux5 regfilemux
(
	.sel2(regfilemux_sel2),
	.sel1(regfilemux_sel1),
	.sel0(regfilemux_sel0),
	.a(alu_out), .b(mem_wdata), .c(adj_adder_out), .d(bytezext_out), .e(pc_out),
	.f(regfilemux_out)
);

//storemux control init
mux2 #(.width(3)) storemux
(
	.sel(storemux_sel),
	.a(ir_sr1_out), .b(ir_dest_out),
	.f(storemux_out)
);

//adj6 unit for alu ops
adj #(.width(6)) adj6
(
	.in(ir_off6_out),
	.out(adj6_out)
);

//ir unit to forward instruction to sending to alu or change pc
ir ir_unit
(
	.clk,
	.load(load_ir),
	.in(mem_wdata),
	.opcode(opcode),
	.dest(ir_dest_out),
	.src1(ir_sr1_out), .src2(ir_sr2_out),
	.imm_sel(imm_sel),
	.imm5(ir_imm5_out),
	.offset6(ir_off6_out),
	.offset9(ir_off9_out),
	.offset11(ir_off11_out),
	.imm4(ir_imm4_out),
   .trap(ir_trap8_out),
  	.a(a_sig),
   .d(d_sig),
   .jsr_sel(jsr_sel)
);

//nzp status comparison to IR nzp unit init
cccomp cccomp_unit
(
	.nzp_query(ir_dest_out),
	.nzp_status(cc_reg_out),
	.to_branch(branch_enable)
);

//cc (reg to hold cc) init
register #(.width(3)) cc_reg 
(
	.clk,
	.load(load_cc),
	.in(gencc_out),
	.out(cc_reg_out)
);

//gencc init
gencc gencc_unit
(
	.in(regfilemux_out),
	.out(gencc_out)
);

//forwards trap address to pc mux
trapzext trapzext_unit
(
	.in(ir_trap8_out),
	.out(trapzext_out)
);

mux2 #(.width(8)) bytemux
(
	.sel(mem_address[0:0]),
	.a(mem_wdata[7:0]),
	.b(mem_wdata[15:8]),
	.f(bytemux_out)
);

//zexts byte gotten from MDR, forwards to register mux
bytezext bytezext_unit
(
	.in(bytemux_out),
	.out(bytezext_out)
);

endmodule : datapath
