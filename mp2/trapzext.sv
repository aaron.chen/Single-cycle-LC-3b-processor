import lc3b_types::*;

/*
 * ZEXT[trapvector] << 1
 */
module trapzext
(
    input lc3b_trap8 in,
    output lc3b_word out
);

assign out = $unsigned({in, 1'b0});

endmodule : trapzext