import lc3b_types::*;

module cccomp
(
    input lc3b_nzp nzp_query, nzp_status,
    output logic to_branch
);

always_comb
begin
	if ((nzp_query[0:0]) && (nzp_status[0:0] == 1))
		to_branch = 1;
	else if ((nzp_query[1:1] == 1) & (nzp_status[1:1] == 1))
		to_branch = 1;
	else if ((nzp_query[2:2] == 1) & (nzp_status[2:2] == 1))
		to_branch = 1;
	else
		to_branch = 0;
end

endmodule : cccomp