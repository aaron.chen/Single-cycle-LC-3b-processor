import lc3b_types::*;

/*
 * ZEXT[byte] 
 */
module bytezext
(
    input logic [7:0] in,
    output lc3b_word out
);

assign out = $unsigned(in);

endmodule : bytezext