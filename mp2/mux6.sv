module mux6 #(parameter width = 16)
(
	input logic sel2,
	input logic sel1,
	input logic sel0,
	input [width-1:0] a, b, c, d, e, f,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel2 == 0 && sel1 == 0 && sel0 == 0)
		out = a;
	else if (sel2 == 0 && sel1 == 0 && sel0 == 1)
		out = b;
	else if (sel2 == 0 && sel1 == 1 && sel0 == 0)
		out = c;
	else if (sel2 == 0 && sel1 == 1 && sel0 == 1)
		out = d;
	else if (sel2 == 1 && sel1 == 0 && sel0 == 0)
		out = e;
	else
		out = f;
end

endmodule : mux6