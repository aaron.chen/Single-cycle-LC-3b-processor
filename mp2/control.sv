import lc3b_types::*; /* Import types defined in lc3b_types.sv */

module control
(
	//clock
	input clk,
	
	//////CPU - DATAPATH///////
	/*pc control signals */
	output logic pcmux_sel1,
	output logic pcmux_sel0,
	output logic load_pc,

	//adj
	output logic adjmux_sel, // normal branch adj9 to adj adder, or jsr offset to adder
	 
	//mar
	output logic marmux_sel1, //mem addr from offset or from pc?
	output logic marmux_sel0, //mem addr from offset or from pc?
	output logic load_mar, //get mar (addr) or not

	//mdr
	output logic mdrmux_sel, //load alu result or data from M[MAR] into MDR?
	output logic load_mdr, //get mdr (data) or not

	//alu
	output lc3b_aluop aluop, //which operation to do
	output logic alumux_sel2, //sr2 or offset or imm (bit 1)
	output logic alumux_sel1, //sr2 or offset or imm (bit 1)
	output logic alumux_sel0, //sr2 or offset or imm (bit 0)

	//reg_file / muxes
	output logic load_regfile, //get regfile data or not
	output logic r7mux_sel, // norm dest or r7
	output logic storemux_sel, //tells if we should use sr1 or dest reg
	output logic regfilemux_sel2, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 1)
	output logic regfilemux_sel1, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 1)
	output logic regfilemux_sel0, // choose what to put in dest reg / gen CC with (choose between alu out, MDR, lea_comped address) (bit 0)


	//ir
	output logic load_ir, // cpu asks to get info in instruction reg or not?
	input lc3b_opcode opcode, //send opcode to cpu
	input logic imm_sel, //tells control whether to use imm val
	input logic jsr_sel, //tells control whether to use pc offset for jsr
	input logic a_sig, //arithmetic shift?
	input logic d_sig, //right or left shift?


	//cccomp
	input logic branch_enable, //tells cpu whether to branch or not

	//cc
	output logic load_cc, // cpu tells to change to new cc or not

	input lc3b_word mem_address,

	//////CPU - MEMORY///////
	input logic mem_resp,
	output logic mem_read,
	output logic mem_write,
	output lc3b_mem_wmask mem_byte_enable
);

enum int unsigned {
	//states
	fetch1,
	fetch2,
	fetch3,
	decode,
	s_add,
	s_and,
	s_not,
	jmp,
	jsr,
	save_pc_to_r7,
	lea,
	shf,
	trap1,
	trap2,
	trap3,
	br,
	br_taken,
	indirect_addr_to_mdr,
	word_calc_addr_to_mar,
	byte_calc_addr_to_mar,
	mem_rdata_to_mar,
	mem_to_mdr,
	mdr_to_dr,
	byte_mdr_to_dr,
	sr_to_mdr,
	mdr_to_m_mar
} state, next_state;

//define state machine for signals
always_comb
begin : state_actions
	//default sigs
	load_pc = 1'b0;
	load_ir = 1'b0;
	load_regfile = 1'b0;
	load_mar = 1'b0;
	load_mdr = 1'b0;
	load_cc = 1'b0;
	pcmux_sel1 = 1'b0;
	pcmux_sel0 = 1'b0;
	storemux_sel = 1'b0;
	alumux_sel2 = 1'b0;
	alumux_sel1 = 1'b0;
	alumux_sel0 = 1'b0;
	regfilemux_sel2 = 1'b0;
	regfilemux_sel1 = 1'b0;
	regfilemux_sel0 = 1'b0;
	marmux_sel1 = 1'b0;
	marmux_sel0 = 1'b0;
	mdrmux_sel = 1'b0;
	aluop = alu_add;
	mem_read = 1'b0;
	mem_write = 1'b0;
	mem_byte_enable = 2'b11;
	r7mux_sel = 1'b0;
	adjmux_sel = 1'b1;

	case(state)
		//which PC address to get?
		fetch1:
			begin
				//MAR <- PC
				marmux_sel1 = 0;
				marmux_sel0 = 1;
				load_mar = 1;
				
				// PC <= PC + 2 
				pcmux_sel1 = 0;
				pcmux_sel0 = 0;
				load_pc = 1; // PC <- PC + 2
			end
		//Read requested address
		fetch2:
			begin
				//MDR <- M[MAR];
				mdrmux_sel = 1;
				load_mdr = 1; 
				mem_read = 1;
			end
		// Load IR found from memory
		fetch3:
			begin
				//IR <- MDR;
				load_ir = 1; 
			end
		decode:
			begin
				; 
			end
		//retrieve requested data into MDR
		mem_to_mdr:
			begin
				//MDR <- M[MAR];
				mdrmux_sel = 1;
				load_mdr = 1;
				mem_read = 1;
			end
		//save requested MDR into DR
		mdr_to_dr:
			begin
				//DR <- MDR;
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 1;
				load_regfile = 1;
				load_cc = 1;
			end
		//?
		br:
			begin
				; //do nothing
			end
		//change next address to desired branched addr
		br_taken:
			begin
				//PC <- PC + SEXT(IR[8:0] << 1); (because addr increases in 2s) (adj9)
				adjmux_sel = 1; //adj9
				pcmux_sel1 = 1;
				pcmux_sel0 = 0; //adj + pc
				load_pc = 1;
			end
		//dump contents of SR into MDR to save soon 
		sr_to_mdr:
			begin
				//MDR <- SR;
				storemux_sel = 1;
				aluop = alu_pass;
				load_mdr = 1;
			end
		//save contents of MDR into M[MAR]
		mdr_to_m_mar:
			begin
				begin
					if (opcode == op_stb && mem_address[0:0] == 0) // if even, write to lower bit
						mem_byte_enable = 2'b01;
					else if (opcode == op_stb && mem_address[0:0] == 1) //or higher bit
						mem_byte_enable = 2'b10;
					else
						mem_byte_enable = 2'b11;
				end
				//M[MAR] <- MDR;
				mem_write = 1;
			end
		//SR1 + SR2
		s_add:
			begin
				if (imm_sel == 0) //DR <- SR1 + SR2;
					begin
						alumux_sel2 = 0;
						alumux_sel1 = 0;
						alumux_sel0 = 0;
					end
				else
					begin
						alumux_sel2 = 0; //DR <- SR1 + imm5;
						alumux_sel1 = 1; //DR <- SR1 + imm5;
						alumux_sel0 = 0;
					end
				aluop = alu_add;
				load_regfile = 1;
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 0;
				load_cc = 1;
			end
		//SR1 & SR2
		s_and:
			begin
				if (imm_sel == 0) //DR <- SR1 & SR2;
					begin
						alumux_sel2 = 0;
						alumux_sel1 = 0;
						alumux_sel0 = 0;
					end
				else
					begin
						alumux_sel2 = 0; 
						alumux_sel1 = 1; //DR <- SR1 & imm5;
						alumux_sel0 = 0;
					end
				aluop = alu_and;
				load_regfile = 1;
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 0;
				load_cc = 1;
			end
		//~SR1
		s_not:
			begin
				//DR <- ~A;
				aluop = alu_not;
				load_regfile = 1;
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 0;
				load_cc = 1;
			end

		//shift left if d == 0, shift arith right if a == 1, else shift logic right
		shf:
			begin
				if (d_sig == 0) //set correct shift
					aluop = alu_sll;
				else if (a_sig == 0)
					aluop = alu_srl;
				else if (a_sig == 1)
					aluop = alu_sra;
				alumux_sel2 = 1; //bring imm4 to alu
				alumux_sel1 = 0;
				alumux_sel0 = 0;
				load_regfile = 1; //set new reg
				regfilemux_sel2 = 0; 
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 0;
				load_cc = 1;
			end
		//for calculating offsets for LDR/STR; Also changes MDR to adjusted mem
		word_calc_addr_to_mar:
			begin
				//MAR <- A + SEXT(IR[5:0] << 1); (because addr increases in 2s)
				alumux_sel2 = 0;
				alumux_sel1 = 0;
				alumux_sel0 = 1;
				aluop = alu_add;
				load_mar = 1;
			end
		//for calculating offsets for LDB/STB; Also changes MDR to adjusted mem
		byte_calc_addr_to_mar:
			begin
				//MAR <- A + SEXT(IR[5:0]);
				alumux_sel2 = 0;
				alumux_sel1 = 1;
				alumux_sel0 = 1; //use imm6 instead
				aluop = alu_add;
				load_mar = 1;
			end
		//jump to val in MDR, gotten from baseR+0
		jmp:
			begin
				aluop = alu_pass;
				pcmux_sel1 = 0;
				pcmux_sel0 = 1;
				load_pc = 1;
			end
		//same calculation always done in br, just transfer it to dest reg; also change cc
		lea:
			begin
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 1;
				regfilemux_sel0 = 0;
				load_regfile = 1;
				load_cc = 1;
			end
		//technically pc+2 => 7;
		save_pc_to_r7:
			begin
				regfilemux_sel2 = 1;
				regfilemux_sel1 = 0;
				regfilemux_sel0 = 0; //bring pc+2 to regfile
				
				r7mux_sel = 1; //make r7 dest reg and load it
				load_regfile = 1;
			end
		//jump to base r (sr1) or offset 11
		jsr:
			begin
				if (jsr_sel == 0) // pc <- baser
					begin
						aluop = alu_pass; //bring baseR to alu out
						pcmux_sel1 = 0;
						pcmux_sel0 = 1; // bring alu to PC
						load_pc = 1;
					end
				else // pc <-- offset11
					begin
						adjmux_sel = 0; // use adj11
						pcmux_sel1 = 1;
						pcmux_sel0 = 0; // bring adj11 to PC
						load_pc = 1;
					end
			end
		// mdr 0-> dest but byte (for ldb)
		byte_mdr_to_dr:
			begin
				regfilemux_sel2 = 0;
				regfilemux_sel1 = 1;
				regfilemux_sel0 = 1; // bring bytezext to regfile
				load_regfile = 1; //save it to dest reg
				load_cc = 1;
			end
		indirect_addr_to_mdr:
			begin
				//MDR <- M[MAR];
				mdrmux_sel = 1;
				load_mdr = 1;
				mem_read = 1;
			end
		mem_rdata_to_mar:
			begin
				marmux_sel1 = 1;
				marmux_sel0 = 1; //data received from indirect address reloaded into MAR
				load_mar = 1;
			end
		trap1:
			begin
				marmux_sel1 = 1;
				marmux_sel0 = 0; //bring trap vector to mar
				load_mar = 1;
			end
		trap2:
			begin
				mdrmux_sel = 1;
				load_mdr = 1;
				mem_read = 1; //bring trap data to MDR
			end
		trap3:
			begin
				pcmux_sel1 = 1;
				pcmux_sel0 = 1;// bring trap data (mdr) to pc
				load_pc = 1;
			end
		default:
			begin
				; //do nothing
			end
	endcase
end

//simplified lc3 state machine
always_comb
begin : next_state_logic
	case(state)
		fetch1:
			begin
				next_state = fetch2;
			end
		fetch2:
			begin
				if (mem_resp == 0)
					next_state = state;
				else
					next_state = fetch3;
			end
		fetch3:
			begin
				next_state = decode;
			end
		decode:
			begin
				case(opcode)
					op_ldr:
						begin
							next_state = word_calc_addr_to_mar;
						end
					op_br:
						begin
							next_state = br;
						end
					op_str:
						begin
							next_state = word_calc_addr_to_mar;
						end
					op_add:
						begin
							next_state = s_add;
						end
					op_and:
						begin
							next_state = s_and;
						end 
					op_not:
						begin
							next_state = s_not;
						end
					op_shf:
						begin
							next_state = shf;
						end
					op_trap:
						begin
							next_state = save_pc_to_r7;
						end
					op_jmp:
						begin
							next_state = jmp;
						end
					op_lea:
						begin
							next_state = lea;
						end
					op_jsr:
						begin
							next_state = save_pc_to_r7;
						end
					op_ldb:
						begin
							next_state = byte_calc_addr_to_mar;
						end
					op_ldi:
						begin
							next_state = word_calc_addr_to_mar;
						end
					op_stb:
						begin
							next_state = byte_calc_addr_to_mar;
						end
					op_sti:
						begin
							next_state = word_calc_addr_to_mar;
						end
					default:
						begin
							next_state = fetch1;
						end
				endcase
			end
		s_add:
			begin
				next_state = fetch1;
			end
		s_and:
			begin
				next_state = fetch1;
			end
		s_not:
			begin
				next_state = fetch1;
			end
		br:
			begin
				if (branch_enable == 1)
					next_state = br_taken;
				else
					next_state = fetch1;
			end
		br_taken:
			begin
				next_state = fetch1;
			end
		word_calc_addr_to_mar:
			begin
				if (opcode == op_ldr)
					next_state = mem_to_mdr;
				else if (opcode == op_str)
					next_state = sr_to_mdr;
				else if (opcode == op_ldi)
					next_state = indirect_addr_to_mdr;
				else if (opcode == op_sti)
					next_state = indirect_addr_to_mdr;
				else
					next_state = fetch1;
			end
		byte_calc_addr_to_mar:
			begin
				if (opcode == op_stb)
					next_state = sr_to_mdr;
				else if (opcode == op_ldb)
					next_state = mem_to_mdr;
				else
					next_state = fetch1;
			end
		mem_to_mdr:
			begin
				if (mem_resp == 0)
					next_state = state;
				else if (opcode == op_ldb)
					next_state = byte_mdr_to_dr;
				else if (opcode == op_ldr)
					next_state = mdr_to_dr;
				else if (opcode == op_ldi)
					next_state = mdr_to_dr;
				else
					next_state = fetch1;
			end
		mdr_to_dr:
			begin
				next_state = fetch1;
			end
		byte_mdr_to_dr:
			begin
				next_state = fetch1;
			end
		sr_to_mdr:
			begin
				next_state = mdr_to_m_mar;
			end
		mdr_to_m_mar:
			begin
				if (mem_resp == 0)
					next_state = state;
				else
					next_state = fetch1;
			end
		jmp:
			begin
				next_state = fetch1;
			end
		lea:
			begin
				next_state = fetch1;
			end
		trap1:
			begin
				next_state = trap2;
			end
		trap2:
			begin
				if (mem_resp == 0)
					next_state = state;
				else
					next_state = trap3;
			end
		trap3:
			begin
				next_state = fetch1;
			end
		jsr:
			begin
				next_state = fetch1;
			end
		save_pc_to_r7:
			begin
				if (opcode == op_jsr)
					next_state = jsr;
				else if (opcode == op_trap)
					next_state = trap1;
				else
					next_state = fetch1;
			end
		shf:
			begin
				next_state = fetch1;
			end
		indirect_addr_to_mdr:
			begin
				if (mem_resp == 0)
					next_state = state;
				else
					next_state = mem_rdata_to_mar;
			end
		mem_rdata_to_mar:
			begin
				if (opcode == op_ldi)
					next_state = sr_to_mdr;
				else if (opcode == op_sti)
					next_state = mem_to_mdr;
				else
					next_state = fetch1;
			end
		default:
			begin
				next_state = fetch1;
			end
	endcase
end

always_ff @(posedge clk)
begin: next_state_assignment
	state <= next_state;
end

endmodule : control
