module mux4 #(parameter width = 16)
(
	input logic sel1,
	input logic sel0,
	input [width-1:0] a, b, c, d,
	output logic [width-1:0] f
);

always_comb
begin
	if (sel1 == 0 && sel0 == 0)
		f = a;
	else if (sel1 == 0 && sel0 == 1)
		f = b;
	else if (sel1 == 1 && sel0 == 0)
		f = c;
	else
		f = d;
end

endmodule : mux4