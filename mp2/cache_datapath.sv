import lc3b_types::*;

//external signals
module cache_datapath
(
	input logic clk,

	// cache data <-> phys mem
	input lc3b_c_line pmem_rdata,
	output lc3b_word pmem_address,
	output lc3b_c_line pmem_wdata,

	//cache data <-> data path
	output lc3b_word mem_rdata,
	input lc3b_word mem_address,
	input lc3b_word mem_wdata,

	//cache data <-> control
	input lc3b_mem_wmask mem_byte_enable,

	//cache datapath <- cache control
	input logic data_arr1_write,
	input logic data_arr0_write,
	input logic tag_arr1_write,
	input logic tag_arr0_write,
	input logic valid_bit_arr1_write,
	input logic valid_bit_arr0_write,
	input logic dirty_bit_arr1_write,
	input logic dirty_bit_arr0_write,
	input logic lru_bit_in,
	input logic lru_bit_arr_write,
	input logic cacheinmux_sel,
	input logic cacheoutmux_sel,
	input logic writebackmux_sel,
	input logic dirty_bit_in,

	//cache datapath -> cache control
	output logic tag1_isvalid,
	output logic tag0_isvalid,
	output logic tag1_hit,
	output logic tag0_hit,
	output logic data1_isdirty,
	output logic data0_isdirty,
	output logic lru_bit_out
);

lc3b_c_line data_arr1_out;
lc3b_c_line data_arr0_out;
lc3b_c_tag tag_arr1_out;
lc3b_c_tag tag_arr0_out;
logic valid_bit_arr1_out;
logic valid_bit_arr0_out;
logic dirty_bit_arr1_out;
logic dirty_bit_arr0_out;

logic tag1comp_out;
logic tag0comp_out;

lc3b_word cacheinwordmux_out;
lc3b_word cacheoutwordmux_out;
lc3b_c_tag writebackmux_out;
lc3b_c_line wordreplacemux_out;
lc3b_c_tag tagoutmux_out;

lc3b_c_line cacheinmux_out;
lc3b_c_line cacheoutmux_out;

assign pmem_address = {writebackmux_out, mem_address[6:0]}; //either the mem addr requested or modified with a previous tag in the case of writeback
assign mem_rdata = cacheoutwordmux_out; //memr_data is abstraction to output of the cache
assign pmem_wdata = cacheoutmux_out; //cacheoutmux_out is abstraction to what we want to write to pmemw_data

assign tag1_hit = tag1comp_out; //tag1_hit abstractoin for comparison mem[first 9] == tag 
assign tag0_hit = tag0comp_out; //tag0_hit abstractoin for comparison mem[first 9] == tag 

assign tag1_isvalid = valid_bit_arr1_out; //tag1_isvalid abstraction for output of valid bit arr
assign tag0_isvalid = valid_bit_arr0_out;//tag1_isvalid abstraction for output of valid bit arr

assign data1_isdirty = dirty_bit_arr1_out; //data1_isdirty abstraction for output of dirty bit arr
assign data0_isdirty = dirty_bit_arr0_out; //data0_isdirty abstraction for output of dirty bit arr

array #(.width(128)) data_arr1
(
	.clk(clk),
	.write(data_arr1_write),
	.index(mem_address[6:4]),
	.datain(cacheinmux_out),
	.dataout(data_arr1_out)
);

array #(.width(128)) data_arr0
(
	.clk(clk),
	.write(data_arr0_write),
	.index(mem_address[6:4]),
	.datain(cacheinmux_out),
	.dataout(data_arr0_out)
);

array #(.width(9)) tag_arr1
(
	.clk(clk),
	.write(tag_arr1_write),
	.index(mem_address[6:4]),
	.datain(mem_address[15:7]),
	.dataout(tag_arr1_out)
);

array #(.width(9)) tag_arr0
(
	.clk(clk),
	.write(tag_arr0_write),
	.index(mem_address[6:4]),
	.datain(mem_address[15:7]),
	.dataout(tag_arr0_out)
);

array #(.width(1)) valid_bit_arr1
(
	.clk(clk),
	.write(valid_bit_arr1_write),
	.index(mem_address[6:4]),
	.datain(1'b1),
	.dataout(valid_bit_arr1_out)
);

array #(.width(1)) valid_bit_arr0
(
	.clk(clk),
	.write(valid_bit_arr0_write),
	.index(mem_address[6:4]),
	.datain(1'b1),
	.dataout(valid_bit_arr0_out)
);

array #(.width(1)) dirty_bit_arr1
(
	.clk(clk),
	.write(dirty_bit_arr1_write),
	.index(mem_address[6:4]),
	.datain(dirty_bit_in),
	.dataout(dirty_bit_arr1_out)
);

array #(.width(1)) dirty_bit_arr0
(
	.clk(clk),
	.write(dirty_bit_arr0_write),
	.index(mem_address[6:4]),
	.datain(dirty_bit_in),
	.dataout(dirty_bit_arr0_out)
);

array #(.width(1)) lru_bit_arr
(
	.clk(clk),
	.write(lru_bit_arr_write),
	.index(mem_address[6:4]),
	.datain(lru_bit_in),
	.dataout(lru_bit_out)
);

comparator #(.width(9)) tag1comp
(
	.a(tag_arr1_out),
	.b(mem_address[15:7]),
	.f(tag1comp_out)
);

comparator #(.width(9)) tag0comp
(
	.a(tag_arr0_out),
	.b(mem_address[15:7]),
	.f(tag0comp_out)
);

mux4 #(.width(16)) cacheinwordmux
(
	.sel1(mem_byte_enable[1]), .sel0(mem_byte_enable[0]),
	.a(cacheoutwordmux_out), //don't change
	.b({cacheoutwordmux_out[15:8],mem_wdata[7:0]}), //change lower byte
	.c({mem_wdata[15:8],cacheoutwordmux_out[7:0]}), //change higher byte
	.d(mem_wdata), //change into given word
	.f(cacheinwordmux_out)
);

mux8 #(.width(128)) wordreplacemux
(
	.sel(mem_address[3:1]),
	.a({cacheoutmux_out[127:16],cacheinwordmux_out}), 
	.b({cacheoutmux_out[127:32],cacheinwordmux_out,cacheoutmux_out[15:0]}),
	.c({cacheoutmux_out[127:48],cacheinwordmux_out,cacheoutmux_out[31:0]}),
	.d({cacheoutmux_out[127:64],cacheinwordmux_out,cacheoutmux_out[47:0]}),
	.e({cacheoutmux_out[127:80],cacheinwordmux_out,cacheoutmux_out[63:0]}),
	.f({cacheoutmux_out[127:96],cacheinwordmux_out,cacheoutmux_out[79:0]}),
	.g({cacheoutmux_out[127:112],cacheinwordmux_out,cacheoutmux_out[95:0]}),
	.h({cacheinwordmux_out,cacheoutmux_out[111:0]}),
	.out(wordreplacemux_out)
);

mux2 #(.width(128)) cacheinmux
(
	.sel(cacheinmux_sel),
	.a(wordreplacemux_out), .b(pmem_rdata),
	.f(cacheinmux_out)
);

mux2 #(.width(128)) cacheoutmux
(
	.sel(cacheoutmux_sel),
	.a(data_arr0_out), .b(data_arr1_out),
	.f(cacheoutmux_out)
);

mux8 #(.width(16)) cacheoutwordmux
(
	.sel(mem_address[3:1]),
	.a(cacheoutmux_out[15:0]), 
	.b(cacheoutmux_out[31:16]),
	.c(cacheoutmux_out[47:32]),
	.d(cacheoutmux_out[63:48]),
	.e(cacheoutmux_out[79:64]),
	.f(cacheoutmux_out[95:80]),
	.g(cacheoutmux_out[111:96]),
	.h(cacheoutmux_out[127:112]),
	.out(cacheoutwordmux_out)
);

mux2 #(.width(9)) tagoutmux
(
	.sel(cacheoutmux_sel),
	.a(tag_arr0_out),
	.b(tag_arr1_out),
	.f(tagoutmux_out)
);

mux2 #(.width(9)) writebackmux
(
	.sel(writebackmux_sel),
	.a(mem_address[15:7]),
	.b(tagoutmux_out),
	.f(writebackmux_out)
);

endmodule : cache_datapath