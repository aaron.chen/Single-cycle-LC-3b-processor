import lc3b_types::*;

module mp2
(
	input clk,

	/* Memory signals */
	input pmem_resp,
	input lc3b_c_line pmem_rdata,
	output pmem_read,
	output pmem_write,
	output lc3b_word pmem_address,
	output lc3b_c_line pmem_wdata
);

///////// CTRL -> DATAPATH
logic ctrl_pcmux_sel1;
logic ctrl_pcmux_sel0;
logic ctrl_load_pc;

//adj
logic ctrl_adjmux_sel; // normal branch adj9 to adj adder, or jsr offset to adder

//mar
logic ctrl_marmux_sel1;  //mem addr from offset or from pc?
logic ctrl_marmux_sel0;  //mem addr from offset or from pc?
logic ctrl_load_mar; //get mar (addr) or not

//mdr
logic ctrl_mdrmux_sel; //load alu result or data from M[MAR] into MDR?
logic ctrl_load_mdr; //get mdr (data) or not

//alu
logic ctrl_r7mux_sel; // normal dest or r7
lc3b_aluop ctrl_aluop; //which operation to do
logic ctrl_alumux_sel2; //sr2 or offset or imm (bit 1)
logic ctrl_alumux_sel1; //sr2 or offset or imm (bit 1)
logic ctrl_alumux_sel0; //sr2 or offset or imm (bit 1)

//reg_file / muxes
logic ctrl_load_regfile; //get regfile data or not
logic ctrl_storemux_sel; //tells if we should use sr1 or dest reg
logic ctrl_regfilemux_sel2; //tells if we load operation result or load stuff in memory to dest reg or comped addr
logic ctrl_regfilemux_sel1; //tells if we load operation result or load stuff in memory to dest reg or comped addr
logic ctrl_regfilemux_sel0; //tells if we load operation result or load stuff in memory to dest reg or comped addr

//ir
logic ctrl_load_ir; // cpu asks to get info in instruction reg or not?

//cc
logic ctrl_load_cc; // cpu tells to change to new cc or not


////DATAPATH -> CTRL
lc3b_opcode data_opcode; //send opcode to cpu

//cccomp
logic data_branch_enable; //tells cpu whether to branch or not

//imm
logic data_imm_sel; //tells control whether to use imm val
logic data_jsr_sel; //tells control whether to use pc offset for jsr
logic data_a_sig; //arithmetic shift?
logic data_d_sig; //right or left shift?



////DATAPATH <-> CACHE
logic mem_resp;
lc3b_word mem_rdata;
logic mem_read;
logic mem_write;
lc3b_mem_wmask mem_byte_enable;
lc3b_word mem_address;
lc3b_word mem_wdata;

//cache datapath <- cache control
logic data_arr1_write;
logic data_arr0_write;
logic tag_arr1_write;
logic tag_arr0_write;
logic valid_bit_arr1_write;
logic valid_bit_arr0_write;
logic dirty_bit_arr1_write;
logic dirty_bit_arr0_write;
logic lru_bit_in;
logic lru_bit_arr_write;
logic cacheinmux_sel;
logic cacheoutmux_sel;

//cache datapath -> cache control
logic tag1_isvalid;
logic tag0_isvalid;
logic tag1_hit;
logic tag0_hit;
logic data1_isdirty;
logic data0_isdirty;
logic lru_bit_out;

logic dirty_bit_in;
logic writebackmux_sel;

datapath mp2_datapath
(
	.clk,
	.pcmux_sel1(ctrl_pcmux_sel1),
	.pcmux_sel0(ctrl_pcmux_sel0),
	.load_pc(ctrl_load_pc),

	//adj
	.adjmux_sel(ctrl_adjmux_sel), // normal branch adj9 to adj adder, or jsr offset to adder
	 
	//mar
	.marmux_sel1(ctrl_marmux_sel1),
	.marmux_sel0(ctrl_marmux_sel0),
	.load_mar(ctrl_load_mar),

	//mdr
	.mdrmux_sel(ctrl_mdrmux_sel), //load alu result or data from M[MAR] into MDR?
	.load_mdr(ctrl_load_mdr), //get mdr (data) or not

	//alu
	.r7mux_sel(ctrl_r7mux_sel),
	.aluop(ctrl_aluop), //which operation to do
	.alumux_sel2(ctrl_alumux_sel2), //sr2 or offset or imm (bit 1)
	.alumux_sel1(ctrl_alumux_sel1), //sr2 or offset or imm (bit 1)
	.alumux_sel0(ctrl_alumux_sel0), //sr2 or offset or imm (bit 1)

	//reg_file / muxes
	.load_regfile(ctrl_load_regfile), //get regfile data or not
	.storemux_sel(ctrl_storemux_sel), //tells if we should use sr1 or dest reg
	.regfilemux_sel2(ctrl_regfilemux_sel2), //tells if we load operation result or load stuff in memory to dest reg
	.regfilemux_sel1(ctrl_regfilemux_sel1), //tells if we load operation result or load stuff in memory to dest reg
	.regfilemux_sel0(ctrl_regfilemux_sel0), //tells if we load operation result or load stuff in memory to dest reg

	//ir
	.load_ir(ctrl_load_ir), // cpu asks to get info in instruction reg or not?
	.opcode(data_opcode), //send opcode to cpu

	//cccomp
	.branch_enable(data_branch_enable), //tells cpu whether to branch or notfile:///home/aychen3/ece411/mp2/mux2.sv
	.imm_sel(data_imm_sel),
	.jsr_sel(data_jsr_sel),
	.a_sig(data_a_sig),
	.d_sig(data_d_sig),


	//cc
	.load_cc(ctrl_load_cc),

	.mem_wdata(mem_wdata), //output from MDR to write to memory
	.mem_address(mem_address), //sends memory to be accessed
	.mem_rdata(mem_rdata) //retrieved data from M[MAR]
);

control mp2_control
(
	.clk,
	/*pc control signals */
	.pcmux_sel1(ctrl_pcmux_sel1),
	.pcmux_sel0(ctrl_pcmux_sel0),
	.load_pc(ctrl_load_pc),
	 
	//adj
	.adjmux_sel(ctrl_adjmux_sel), // normal branch adj9 to adj adder, or jsr offset to adder

	//mar
	.marmux_sel1(ctrl_marmux_sel1), //mem addr from offset or from pc?
	.marmux_sel0(ctrl_marmux_sel0), //mem addr from offset or from pc?
	.load_mar(ctrl_load_mar), //get mar (addr) or not

	//mdr
	.mdrmux_sel(ctrl_mdrmux_sel), //load alu result or data from M[MAR] into MDR?
	.load_mdr(ctrl_load_mdr), //get mdr (data) or not

	//alu
	.r7mux_sel(ctrl_r7mux_sel),
	.aluop(ctrl_aluop), //which operation to do
	.alumux_sel2(ctrl_alumux_sel2), //sr2 or offset or imm (bit 1)
	.alumux_sel1(ctrl_alumux_sel1), //sr2 or offset or imm (bit 1)
	.alumux_sel0(ctrl_alumux_sel0), //sr2 or offset or imm (bit 1)

	//reg_file / muxes
	.load_regfile(ctrl_load_regfile), //get regfile data or not
	.storemux_sel(ctrl_storemux_sel), //tells if we should use sr1 or dest reg
	.regfilemux_sel2(ctrl_regfilemux_sel2), //tells if we load operation result or load stuff in memory to dest reg
	.regfilemux_sel1(ctrl_regfilemux_sel1), //tells if we load operation result or load stuff in memory to dest reg
	.regfilemux_sel0(ctrl_regfilemux_sel0), //tells if we load operation result or load stuff in memory to dest reg

	//ir
	.load_ir(ctrl_load_ir), // cpu asks to get info in instruction reg or not?
	.opcode(data_opcode), //send opcode to cpu

	//cccomp
	.branch_enable(data_branch_enable), //tells cpu whether to branch or not
	.imm_sel(data_imm_sel),
	.jsr_sel(data_jsr_sel),
	.a_sig(data_a_sig),
	.d_sig(data_d_sig),

	//cc
	.load_cc(ctrl_load_cc), // cpu tells to change to new cc or not

	.mem_resp(mem_resp),
	.mem_read(mem_read),
	.mem_write(mem_write),
	.mem_byte_enable(mem_byte_enable),

	
	.mem_address(mem_address) //sends memory to be accessed
);

cache_datapath mp2_cache_datapath
(
	.clk(clk),

	// cache data <-> phys mem
	.pmem_rdata(pmem_rdata),
	.pmem_address(pmem_address),
	.pmem_wdata(pmem_wdata),

	//cache data <-> data path
	.mem_rdata(mem_rdata),
	.mem_address(mem_address),
	.mem_wdata(mem_wdata),

	//cache data <-> control
	.mem_byte_enable(mem_byte_enable),

	//cache datapath <- cache control
	.data_arr1_write(data_arr1_write),
	.data_arr0_write(data_arr0_write),
	.tag_arr1_write(tag_arr1_write),
	.tag_arr0_write(tag_arr0_write),
	.valid_bit_arr1_write(valid_bit_arr1_write),
	.valid_bit_arr0_write(valid_bit_arr0_write),
	.dirty_bit_arr1_write(dirty_bit_arr1_write),
	.dirty_bit_arr0_write(dirty_bit_arr0_write),
	.lru_bit_in(lru_bit_in),
	.lru_bit_arr_write(lru_bit_arr_write),
	.cacheinmux_sel(cacheinmux_sel),
	.cacheoutmux_sel(cacheoutmux_sel),

	//cache datapath -> cache control
	.tag1_isvalid(tag1_isvalid),
	.tag0_isvalid(tag0_isvalid),
	.tag1_hit(tag1_hit),
	.tag0_hit(tag0_hit),
	.data1_isdirty(data1_isdirty),
	.data0_isdirty(data0_isdirty),
	.lru_bit_out(lru_bit_out),
	.dirty_bit_in(dirty_bit_in),
	.writebackmux_sel(writebackmux_sel)
);

cache_control mp2_cache_control
(
	.clk(clk),

	//cache control <-> phys mem
	.pmem_resp(pmem_resp),
	.pmem_read(pmem_read),
	.pmem_write(pmem_write),

	//cache control <-> data control
	.mem_resp(mem_resp),
	.mem_read(mem_read),
	.mem_write(mem_write),

	//cache control <- cache datapath
	.data_arr1_write(data_arr1_write),
	.data_arr0_write(data_arr0_write),
	.tag_arr1_write(tag_arr1_write),
	.tag_arr0_write(tag_arr0_write),
	.valid_bit_arr1_write(valid_bit_arr1_write),
	.valid_bit_arr0_write(valid_bit_arr0_write),
	.dirty_bit_arr1_write(dirty_bit_arr1_write),
	.dirty_bit_arr0_write(dirty_bit_arr0_write),
	.lru_bit_arr_write(lru_bit_arr_write),
	.lru_bit_in(lru_bit_in),
	.cacheinmux_sel(cacheinmux_sel),
	.cacheoutmux_sel(cacheoutmux_sel),

	//cache control -> cache data
	.tag1_isvalid(tag1_isvalid),
	.tag0_isvalid(tag0_isvalid),
	.tag1_hit(tag1_hit),
	.tag0_hit(tag0_hit),
	.data1_isdirty(data1_isdirty),
	.data0_isdirty(data0_isdirty),
	.lru_bit_out(lru_bit_out), //0 if tag0 is least recently used; 1 if tag1 is least recently used
	.dirty_bit_in(dirty_bit_in),
	.writebackmux_sel(writebackmux_sel)
);

endmodule : mp2
