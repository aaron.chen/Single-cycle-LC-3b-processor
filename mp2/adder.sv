module adder #(parameter width = 16)
(
	input [width-1:0] x, y,
	output logic [width-1:0] sum
);

always_comb
begin
	sum = x + y;
end
	
endmodule : adder