import lc3b_types::*;

module r7mux
(
	input sel,
	input lc3b_reg normal_dest, 
	output lc3b_reg f
);

always_comb
begin
	if (sel == 0)
		f = normal_dest;
	else
		f = 3'b111; //r7
end

endmodule : r7mux