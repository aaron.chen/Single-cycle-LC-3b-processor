import lc3b_types::*;

module cache_control
(
	input clk,

	//cache control <-> phys mem
	input logic pmem_resp,
	output logic pmem_read,
	output logic pmem_write,

	//cache control <-> data control
	output logic mem_resp,
	input logic mem_read,
	input logic mem_write,

	//cache control <- cache datapath
	output logic data_arr1_write,
	output logic data_arr0_write,
	output logic tag_arr1_write,
	output logic tag_arr0_write,
	output logic valid_bit_arr1_write,
	output logic valid_bit_arr0_write,
	output logic dirty_bit_arr1_write,
	output logic dirty_bit_arr0_write,
	output logic dirty_bit_in,
	output logic lru_bit_arr_write,
	output logic lru_bit_in,
	output logic cacheinmux_sel,
	output logic cacheoutmux_sel,
	output logic writebackmux_sel,

	//cache control -> cache data
	input logic tag1_isvalid,
	input logic tag0_isvalid,
	input logic tag1_hit,
	input logic tag0_hit,
	input logic data1_isdirty,
	input logic data0_isdirty,
	input logic lru_bit_out //0 if tag0 is least recently used; 1 if tag1 is least recently used
);

enum int unsigned
{
	hit,
	read_into_cache1,
	read_into_cache2,
	writeback
} state, next_state;

//define state actions for signals
always_comb
begin : state_actions
	mem_resp = 1'b0; //by default don't consider mem to have responded
	pmem_read = 1'b0; //by default don't ask for phys mem
	pmem_write = 1'b0;

	//by default don't write
	data_arr1_write = 1'b0;
	data_arr0_write = 1'b0;
	tag_arr1_write = 1'b0;
	tag_arr0_write = 1'b0;
	valid_bit_arr1_write = 1'b0;
	valid_bit_arr0_write = 1'b0;
	dirty_bit_arr1_write = 1'b0;
	dirty_bit_arr0_write = 1'b0;

	lru_bit_in = 1'b0;
	lru_bit_arr_write = 1'b0;
	dirty_bit_in = 1'b0;

	cacheinmux_sel = 1'b0; //by default memw_data will go into cache data
	cacheoutmux_sel = tag1_hit; //by default, if tag1 is hit, we also want to read out from data_array1 unless we writeback
	writebackmux_sel = 1'b0;

	case(state)
		hit: //hit is the only state to give mem_resp = 1; all states must eventually come to hit
		begin

			//if read hit
			if (mem_read == 1 && ((tag1_hit == 1 && tag1_isvalid == 1) || (tag0_hit == 1 && tag0_isvalid == 1))) 
			begin
				//update lru  // if miss, we will adjust cache, come back to hit, and then update lru
				if (tag1_hit == 1)
				begin
					lru_bit_in = 1'b0; //access way1, so way0 is least recent
					lru_bit_arr_write = 1'b1;
				end
				else
				begin
					lru_bit_in = 1'b1; //access way0, so way1 is least recent
					lru_bit_arr_write = 1'b1;
				end	

				mem_resp = 1'b1; //say mem has responded
			end
			//if write hit
			else if (mem_write == 1 && ((tag1_hit == 1 && tag1_isvalid == 1) || (tag0_hit == 1 && tag0_isvalid == 1)))
			begin
				//update lru  // if miss, we will adjust cache, come back to hit, and then update lru
				if (tag1_hit == 1)
				begin
					lru_bit_in = 1'b0; //access way1, so way0 is least recent
					lru_bit_arr_write = 1'b1;
				end
				else
				begin
					lru_bit_in = 1'b1; //access way0, so way1 is least recent
					lru_bit_arr_write = 1'b1;
				end		

				cacheinmux_sel = 1'b0; //send mem_wdata to cache
				if (tag1_hit == 1) //change which data arr to write depending on hit
				begin
					data_arr1_write = 1'b1;
					data_arr0_write = 1'b0;
					dirty_bit_in = 1'b1;
					dirty_bit_arr1_write = 1'b1;
					dirty_bit_arr0_write = 1'b0;
				end
				else
				begin
					data_arr1_write = 1'b0;
					data_arr0_write = 1'b1;
					dirty_bit_in = 1'b1;
					dirty_bit_arr1_write = 1'b0;
					dirty_bit_arr0_write = 1'b1;
				end
				mem_resp = 1'b1; //say mem has responded
			end
			else if (mem_write == 1)
			begin
				if (data1_isdirty == 0 && lru_bit_out == 1)
				begin
					lru_bit_in = 1'b0; //access way1, so way0 is least recent
					lru_bit_arr_write = 1'b1;
					data_arr1_write = 1'b1;
					dirty_bit_in = 1'b1;
					dirty_bit_arr1_write = 1'b1;
					tag_arr1_write = 1'b1;
					mem_resp = 1'b1; //say mem has responded
				end
				else if (data0_isdirty == 0 && lru_bit_out == 0) 
				begin
					lru_bit_in = 1'b1; //access way0, so way1 is least recent
					lru_bit_arr_write = 1'b1;
					data_arr0_write = 1'b1;
					tag_arr0_write = 1'b1;
					dirty_bit_in = 1'b1;
					dirty_bit_arr0_write = 1'b1;
					mem_resp = 1'b1; //say mem has responded
				end
			end
		end
		read_into_cache1:
		begin
			cacheinmux_sel = 1'b1;
			pmem_read = 1'b1;

			if (tag1_isvalid == 0) // tag 1 invalid, read to data1;make tag1 valid/replace tag1
			begin
				data_arr1_write = 1'b1;
				tag_arr1_write = 1'b1;
			end
			else if (tag0_isvalid == 0) //tag 0 invalid, read to data0; make tag0 valid/replace tag0
			begin
				data_arr0_write = 1'b1;
				tag_arr0_write = 1'b1;
			end
			else if (lru_bit_out == 1'b1) //way1 least recent, read to data1; replace tag1
			begin
				data_arr1_write = 1'b1;
				tag_arr1_write = 1'b1;
			end
			else if (lru_bit_out == 1'b0) //way0 least recent, read to data0; replace tag0
			begin
				data_arr0_write = 1'b1;
				tag_arr0_write = 1'b1;
			end
		end
		read_into_cache2:
		begin
			cacheinmux_sel = 1'b1;
			dirty_bit_in = 1'b0; //clean data
			
			//check if either is invalid first, then check lru
			if (tag1_isvalid == 0) // tag 1 invalid, read to data1;make tag1 valid/replace tag1
			begin
				valid_bit_arr1_write = 1'b1;
				lru_bit_in = 1'b0; //access way1, so way0 is least recent
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr1_write = 1'b1;
			end
			else if (tag0_isvalid == 0) //tag 0 invalid, read to data0; make tag0 valid/replace tag0
			begin
				valid_bit_arr0_write = 1'b1;
				lru_bit_in = 1'b1; //access way0, so way1 is least recent
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr0_write = 1'b1;
			end	
			else if (lru_bit_out == 1'b1) //way1 least recent, read to data1
			begin
				lru_bit_in = 1'b0; //access way1, so way0 is now least recent
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr1_write = 1'b1;
			end
			else if (lru_bit_out == 1'b0) //way0 least recent, read to data0; replace tag0
			begin
				lru_bit_in = 1'b1; //access way0, so way1 is now least recent
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr0_write = 1'b1;
			end
		end
		writeback:
		begin
			writebackmux_sel = 1'b1;
			// only two situations to get here: either both tags are valid, but one not dirty; or both tags valid, but both dirty
			//b/c when dirty bit set, valid bit also set. Can't get here both dirty, but one not valid
			if (lru_bit_out == 1'b1) //way1 least recent, this will get overwritten, so writeback
			begin
					cacheoutmux_sel = 1'b1;
			end
			else
			begin
				cacheoutmux_sel = 1'b0;
			end
			pmem_write = 1'b1;
		end //otherwise don't have to do anything, go straight to read / write to cache
	endcase
end

//state transitions for signals
always_comb
begin : next_state_logic
	next_state = hit;
	case(state)
		hit:
		begin
			//first check if it's a hit (read)
			if (mem_read == 1 && ((tag1_hit == 1 && tag1_isvalid == 1) || (tag0_hit == 1 && tag0_isvalid == 1)))
			begin // do nothing
				next_state = hit;
			end //check if it's a hit (write)
			else if (mem_write == 1 && ((tag1_hit == 1 && tag1_isvalid == 1) || (tag0_hit == 1 && tag0_isvalid == 1)))
			begin
				next_state = hit; // do nothing
			end
			else // if miss
			begin
				//if one tag isn't valid, no need to writeback, go immediately to read into cache
				if ((mem_read == 1) && (tag1_isvalid == 0 || tag0_isvalid == 0))
				begin
					next_state = read_into_cache1;
				end
				//if one data isn't dirty_bit_arr0_write, no need to writeback, go immediately to read into cache
				else if (mem_write == 1 && (data1_isdirty == 0 || data0_isdirty == 0))
				begin
					next_state = read_into_cache1;
				end
				//else write back if LRU way is dirty and valid
				else if (mem_read == 1 || mem_write == 1)
				begin
					next_state = writeback;
				end
			end
		end
		read_into_cache1:
		begin
			if (pmem_resp == 0)
				next_state = state;
			else
				next_state = read_into_cache2;
		end
		read_into_cache2:
		begin
			next_state = hit;
		end
		writeback:
		begin
			if (lru_bit_out == 1 && data1_isdirty == 1 && tag1_isvalid == 1)
				if (pmem_resp == 0)
					next_state = state; 
				else
					next_state = read_into_cache1;
			else if (lru_bit_out == 0 && data0_isdirty == 1 && tag0_isvalid == 1)
				if (pmem_resp == 0)
					next_state = state; 
				else
					next_state = read_into_cache1;
			else
				next_state = read_into_cache1;
		end
	endcase
end

always_ff @(posedge clk)
begin: next_state_assignment
	state <= next_state;
end

endmodule : cache_control