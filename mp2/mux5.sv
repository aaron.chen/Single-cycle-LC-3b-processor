module mux5 #(parameter width = 16)
(
	input logic sel2,
	input logic sel1,
	input logic sel0,
	input [width-1:0] a, b, c, d, e,
	output logic [width-1:0] f
);

always_comb
begin
	if (sel2 == 0 && sel1 == 0 && sel0 == 0)
		f = a;
	else if (sel2 == 0 && sel1 == 0 && sel0 == 1)
		f = b;
	else if (sel2 == 0 && sel1 == 1 && sel0 == 0)
		f = c;
	else if (sel2 == 0 && sel1 == 1 && sel0 == 1)
		f = d;
	else
		f = e;
end

endmodule : mux5