import lc3b_types::*;

/*
 * ZEXT[in]
 */
module zext #(parameter in_width = 4, out_width)
(
    input logic [in_width-1:0] in,
    output logic [out_width-1:0] out
);

assign out = $unsigned(in);

endmodule : zext